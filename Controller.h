#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <iostream>
#include <GL/freeglut.h>
#include "AppController.h"

void drawCartesianPlane() {
    // set the color to black
    glColor3f(0.0f, 0.0f, 0.0f);

    // draw xy-axis
    glBegin(GL_LINES);
        glVertex2f(-1.0f, 0.0f);
        glVertex2f(1.0f, 0.0f);

        glVertex2f(0.0f, 1.0f);
        glVertex2f(0.0f, -1.0f);
    glEnd();

    // draw the x-axis tick marks
    for (float i = -1.0f; i < 1.0f; i += 0.1f) {
        glBegin(GL_LINES);
            glVertex2f(i, 0.025f);
            glVertex2f(i, -0.025f);
        glEnd();
    }

    // draw the y-axis tick marks
    for (float i = -1.0f; i < 1.0f; i += 0.1f) {
        glBegin(GL_LINES);
            glVertex2f(0.025f, i);
            glVertex2f(-0.025f, i);
        glEnd();
    }
}

// drawing red point
void drawRedPoint() {
    glPointSize(20.0f);
    glColor3f(1.0f, 0.0f, 0.0f);

    glBegin(GL_POINTS);
        glVertex2f(0.5f, 0.5f);
    glEnd();
}

// drawing green cross
void drawCross() {
    glLineWidth(2.0f);
    glColor3f(0.0f, 1.0f, 0.0f);

    glBegin(GL_LINES);
        glVertex2f(-0.8f, 0.2f);
        glVertex2f(-0.2f, 0.8f);

        glVertex2f(-0.8f, 0.8f);
        glVertex2f(-0.2f, 0.2f);
    glEnd();
}

// drawing blue square
void drawSquare() {
    glColor3f(0.0f, 0.0f, 1.0f);

    glBegin(GL_POLYGON);
        glVertex2f(-0.4f, -0.4f);
        glVertex2f(-0.6f, -0.4f);
        glVertex2f(-0.6f, -0.6f);
        glVertex2f(-0.4f, -0.6f);
    glEnd();
}

// drawing magenta triangle
void drawTriangle() {
    glColor3f(1.0f, 0.0f, 1.0f);

    glBegin(GL_POLYGON);
        glVertex2f(0.5f, -0.4f);
        glVertex2f(0.3f, -0.6f);
        glVertex2f(0.7f, -0.6f);
    glEnd();
}

class Controller : public AppController {

public:
    Controller(){
        // Initialize your state variables
    }

    void render() {
        // Render some graphics

        drawCartesianPlane();
        drawRedPoint();
        drawCross();
        drawSquare();
        drawTriangle();
    }

    ~Controller(){
        // Release memory
    }
};

#endif